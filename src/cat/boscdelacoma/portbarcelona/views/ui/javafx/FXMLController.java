/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.boscdelacoma.portbarcelona.views.ui.javafx;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.Vaixell;
import cat.boscdelacoma.portbarcelona.views.ui.test.PortBarcelona;
import static java.lang.String.valueOf;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author xavier
 */
public class FXMLController implements Initializable {
    Vaixell vaixell = new Vaixell();

    @FXML
    private TextArea textArea;
    @FXML
    private Button btn;
    @FXML
    private Text data;
    @FXML
    private Text volMax;
    @FXML
    private Text volMin;
    @FXML
    private Text volMitj;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
            LocalDateTime now = LocalDateTime.now();  
            data.setText(dtf.format(now));
            
            PortBarcelona.carregarVaixell(vaixell);
 
    }    
    
    @FXML
    private void btnClick(ActionEvent event) {
        textArea.appendText("MOSTRAR CONTENIDORS VAIXELL");
        textArea.appendText("\n---------------------------");
        Contenidor contenidor;
        int countMercaderies = 0;


        for (int i = 0; i < vaixell.getnContenidors(); i++) {
            contenidor = vaixell.getContenidor(i);
            System.out.printf("%s\n", contenidor.getNumSerie());
            countMercaderies += contenidor.getnMercaderies();
        }
        textArea.appendText("\n\nTOTAL MERCADERIES: "+ countMercaderies);
        
        
        
        textArea.appendText("\n\nDESCARREGAR VAIXELL");
        textArea.appendText("\n-------------------");

        textArea.appendText("\nCONTENIDOR         VOLUM");
        textArea.appendText("\n------------------------\n");

        textArea.appendText("\nVOLUM TOTAL: " + vaixell.getVolum()+ " m3\n");

        volMax.setText(valueOf(vaixell.getMaxVolum()));
        volMin.setText(valueOf(vaixell.getMinVolum()));
        volMitj.setText(valueOf(vaixell.getMitjaVolum()));
        
        vaixell.descarregar();
        
        
        
    }

    
}
